import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Message } from '@shared/interfaces/message';
import { Room, RoomWithMessages } from '@shared/interfaces/room';
import { User } from '@shared/interfaces/user';

@Injectable({
  providedIn: 'root',
})
export class ChatApiService {
  constructor(private http: HttpClient) {}

  getRooms() {
    return this.http.get<Room[]>(`${environment.apiUrl}/api/rooms`);
  }

  getRoom(roomId: string) {
    return this.http.get<RoomWithMessages>(`${environment.apiUrl}/api/rooms/${roomId}`);
  }

  createRoom(name: string, description?: string, password?: string) {
    return this.http.post<Room>(`${environment.apiUrl}/api/rooms`, { name, description, password });
  }

  createMessage(roomId: string, senderId: string, message: string) {
    return this.http.post<Message>(`${environment.apiUrl}/api/rooms/${roomId}/messages`, { senderId, message });
  }

  createUser(name: string, email: string, password: string) {
    return this.http.post<{ status: string }>(`${environment.apiUrl}/api/users/signup`, { name, email, password });
  }

  loginUser(email: string, password: string) {
    return this.http.post<{ status: string }>(`${environment.apiUrl}/api/users/login`, { email, password });
  }

  logoutUser() {
    return this.http.post<{ status: string }>(`${environment.apiUrl}/api/users/logout`, {});
  }

  getLoggedUser() {
    return this.http.get<User>(`${environment.apiUrl}/api/users/me`);
  }

  getUsers() {
    return this.http.get<User[]>(`${environment.apiUrl}/api/users`);
  }
}
