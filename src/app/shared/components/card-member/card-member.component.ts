import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-member',
  templateUrl: './card-member.component.html',
  styleUrls: ['./card-member.component.scss'],
})
export class CardMemberComponent {
  @Input() avatar = '';
  @Input() name = '';
  @Input() isOnline: boolean | undefined;
}
