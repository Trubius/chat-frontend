import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-room',
  templateUrl: './card-room.component.html',
  styleUrls: ['./card-room.component.scss'],
})
export class CardRoomComponent {
  @Input() name = '';
  @Input() description = '';
  @Input() roomId = '';
}
