import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthFacade } from '@modules/auth/store/facade';
import { map, skipWhile } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class CheckUserGuard implements CanActivate {
  constructor(private authFacade: AuthFacade, private router: Router) {}

  canActivate() {
    this.authFacade.getLoggedUser(false);
    return this.authFacade.user$.pipe(
      skipWhile(user => user === undefined),
      map(user => {
        if (!user) {
          void this.router.navigate(['/']);
          return false;
        }
        return true;
      }),
    );
  }
}
