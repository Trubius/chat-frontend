/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CardMemberComponent } from '@shared/components/card-member/card-member.component';
import { CardRoomComponent } from '@shared/components/card-room/card-room.component';
import { PageNotFoundComponent } from '@shared/components/page-not-found/page-not-found.component';
import { CardMessageComponent } from './components/card-message/card-message.component';

const COMPONENTS = [PageNotFoundComponent, CardRoomComponent, CardMemberComponent, CardMessageComponent];

@NgModule({
  imports: [CommonModule, RouterModule],
  exports: [...COMPONENTS],
  declarations: [...COMPONENTS],
})
export class SharedModule {}
