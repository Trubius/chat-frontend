import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from '@shared/components/page-not-found/page-not-found.component';
import { LoginGuard } from '@shared/guards/login.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'rooms',
    loadChildren: async () => import('./modules/chat/chat.module').then(m => m.ChatModule),
  },
  {
    path: 'signup',
    loadChildren: async () => import('./modules/signup/signup.module').then(m => m.SignupModule),
  },
  {
    path: 'login',
    loadChildren: async () => import('./modules/auth/auth.module').then(m => m.AuthModule),
    canActivate: [LoginGuard],
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
