import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ChatEffects } from './effects';
import { ChatFacade } from './facade';
import { chatReducer } from './reducer';
import { CHAT_STORE_KEY } from './selectors';

@NgModule({
  imports: [CommonModule, StoreModule.forFeature(CHAT_STORE_KEY, chatReducer), EffectsModule.forFeature([ChatEffects])],
  providers: [ChatFacade],
})
export class ChatStoreModule {}
