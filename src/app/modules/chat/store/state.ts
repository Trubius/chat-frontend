import { Room, RoomWithMessages } from '@shared/interfaces/room';
import { User } from '@shared/interfaces/user';

export const initialChatState: ChatState = {
  users: [],
  rooms: [],
  selectedRoom: {
    id: '',
    name: '',
    description: '',
    password: '',
    messages: [],
  },
};

export interface ChatState {
  users: User[];
  rooms: Room[];
  selectedRoom: RoomWithMessages;
}
