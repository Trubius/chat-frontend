import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Message } from '@shared/interfaces/message';
import * as chatActions from './actions';
import * as chatSelectors from './selectors';

@Injectable()
export class ChatFacade {
  rooms$ = this.store.select(chatSelectors.getRooms);
  selectedRoom$ = this.store.select(chatSelectors.getRoom);
  users$ = this.store.select(chatSelectors.getUsers);

  constructor(private store: Store) {}

  getRoomList() {
    this.store.dispatch(chatActions.getRoomList());
  }

  getRoom(roomId: string) {
    this.store.dispatch(chatActions.getRoom({ roomId }));
  }

  createRoom(name: string, description?: string, password?: string) {
    this.store.dispatch(chatActions.createRoom({ name, description, password }));
  }

  createMessage(roomId: string, senderId: string, message: string) {
    this.store.dispatch(chatActions.createMessage({ roomId, senderId, message }));
  }

  addMessage(message: Message) {
    this.store.dispatch(chatActions.addMessage({ message }));
  }

  getUserList() {
    this.store.dispatch(chatActions.getUserList());
  }
}
