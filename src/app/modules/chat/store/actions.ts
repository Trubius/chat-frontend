import { createAction, props } from '@ngrx/store';
import { Message } from '@shared/interfaces/message';
import { Room, RoomWithMessages } from '@shared/interfaces/room';
import { User } from '@shared/interfaces/user';

export const getRoomList = createAction('[Chat] Get Rooms Fetch');
export const getRoomListSuccess = createAction('[Chat] Get Rooms Success', props<{ rooms: Room[] }>());
export const getRoomListFail = createAction('[Chat] Get Rooms Fail', props<{ error: string }>());

export const getRoom = createAction('[Chat] Get Room Fetch', props<{ roomId: string }>());
export const getRoomSuccess = createAction('[Chat] Get Room Success', props<{ selectedRoom: RoomWithMessages }>());
export const getRoomFail = createAction('[Chat] Get Room Fail', props<{ error: string }>());

export const createRoom = createAction(
  '[Chat] Create Room Fetch',
  props<{ name: string; description?: string; password?: string }>(),
);
export const createRoomSuccess = createAction('[Chat] Create Room Success', props<{ room: Room }>());
export const createRoomFail = createAction('[Chat] Create Room Fail', props<{ error: string }>());

export const createMessage = createAction(
  '[Chat] Create Message Fetch',
  props<{ roomId: string; senderId: string; message: string }>(),
);
export const createMessageSuccess = createAction('[Chat] Create Message Success', props<{ message: Message }>());
export const createMessageFail = createAction('[Chat] Create Message Fail', props<{ error: string }>());

export const addMessage = createAction('[Chat] Add Message', props<{ message: Message }>());

export const getUserList = createAction('[Chat] Get Users Fetch');
export const getUserListSuccess = createAction('[Chat] Get Users Success', props<{ users: User[] }>());
export const getUserListFail = createAction('[Chat] Get Users Fail', props<{ error: string }>());
