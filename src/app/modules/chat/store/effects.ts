import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ChatApiService } from '@app/services/api/chat-api.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as chatActions from './actions';

@Injectable()
export class ChatEffects {
  getRooms$ = createEffect(() =>
    this.actions$.pipe(
      ofType(chatActions.getRoomList),
      switchMap(() => {
        return this.chatApiService.getRooms().pipe(
          map(rooms => chatActions.getRoomListSuccess({ rooms })),
          catchError((error: HttpErrorResponse) => of(chatActions.getRoomListFail({ error: error.message }))),
        );
      }),
    ),
  );

  getRoom$ = createEffect(() =>
    this.actions$.pipe(
      ofType(chatActions.getRoom),
      switchMap(({ roomId }) => {
        return this.chatApiService.getRoom(roomId).pipe(
          map(selectedRoom => chatActions.getRoomSuccess({ selectedRoom })),
          catchError((error: HttpErrorResponse) => of(chatActions.getRoomListFail({ error: error.message }))),
        );
      }),
    ),
  );

  createRoom$ = createEffect(() =>
    this.actions$.pipe(
      ofType(chatActions.createRoom),
      switchMap(({ name, description, password }) => {
        return this.chatApiService.createRoom(name, description, password).pipe(
          map(room => chatActions.createRoomSuccess({ room })),
          catchError((error: HttpErrorResponse) => of(chatActions.getRoomListFail({ error: error.message }))),
        );
      }),
    ),
  );

  createMessage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(chatActions.createMessage),
      switchMap(({ roomId, senderId, message }) => {
        return this.chatApiService.createMessage(roomId, senderId, message).pipe(
          map(newMessage => chatActions.createMessageSuccess({ message: newMessage })),
          catchError((error: HttpErrorResponse) => of(chatActions.getRoomListFail({ error: error.message }))),
        );
      }),
    ),
  );

  getUsers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(chatActions.getUserList),
      switchMap(() => {
        return this.chatApiService.getUsers().pipe(
          map(users => chatActions.getUserListSuccess({ users })),
          catchError((error: HttpErrorResponse) => of(chatActions.getUserListFail({ error: error.message }))),
        );
      }),
    ),
  );

  constructor(private actions$: Actions, private chatApiService: ChatApiService) {}
}
