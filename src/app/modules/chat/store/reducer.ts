import { Action, createReducer, on } from '@ngrx/store';
import * as chatActions from './actions';
import { ChatState, initialChatState } from './state';

export const reducer = createReducer(
  initialChatState,
  on(chatActions.getRoomListSuccess, (state, { rooms }) => ({
    ...state,
    rooms,
  })),
  on(chatActions.getRoomSuccess, (state, { selectedRoom }) => ({
    ...state,
    selectedRoom,
  })),
  on(chatActions.createRoomSuccess, (state, { room }) => ({
    ...state,
    rooms: [...state.rooms, room],
  })),
  on(chatActions.addMessage, (state, { message }) => ({
    ...state,
    selectedRoom: { ...state.selectedRoom, messages: [...state.selectedRoom.messages, message] },
  })),
  on(chatActions.getUserListSuccess, (state, { users }) => ({
    ...state,
    users,
  })),
);

export function chatReducer(state: ChatState, action: Action) {
  return reducer(state, action);
}
