import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ChatState } from './state';

export const CHAT_STORE_KEY = 'chat';

const featureSelector = createFeatureSelector<ChatState>(CHAT_STORE_KEY);

export const getRooms = createSelector(featureSelector, state => state.rooms);

export const getRoom = createSelector(featureSelector, state => state.selectedRoom);

export const getUsers = createSelector(featureSelector, state => state.users);
