import { Component, OnInit } from '@angular/core';
import { AuthFacade } from '@modules/auth/store/facade';
import { ChatFacade } from '@modules/chat/store/facade';
import { Socket } from 'ngx-socket-io';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-chat-room-list',
  templateUrl: './chat-room-list.component.html',
  styleUrls: ['./chat-room-list.component.scss'],
})
export class ChatRoomListComponent implements OnInit {
  rooms$ = this.chatFacade.rooms$;
  users$ = combineLatest([this.chatFacade.users$, this.socket.fromEvent<Record<string, string>>('activeUsers')]).pipe(
    map(([users, activeUsers]) => {
      const activeUserIds = Object.values(activeUsers);
      return users.map(user => ({ ...user, isOnline: activeUserIds.includes(user.id) }));
    }),
  );

  constructor(private chatFacade: ChatFacade, private authFacade: AuthFacade, private socket: Socket) {}

  ngOnInit() {
    this.chatFacade.getRoomList();
    this.chatFacade.getUserList();
  }

  createRoom() {
    this.chatFacade.createRoom('chat room', 'description');
  }

  logout() {
    this.authFacade.logoutUser();
  }
}
