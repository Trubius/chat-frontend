import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuthFacade } from '@modules/auth/store/facade';
import { ChatFacade } from '@modules/chat/store/facade';
import { Message } from '@shared/interfaces/message';
import { Socket } from 'ngx-socket-io';
import { distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';

interface MessageFormValue {
  message: string;
}
@Component({
  selector: 'app-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.scss'],
})
export class ChatRoomComponent implements OnInit, AfterViewChecked {
  messageForm = new FormGroup({
    message: new FormControl(''),
  });

  room$ = this.chatFacade.selectedRoom$;
  user$ = this.authFacade.user$;
  chatMessage$ = this.route.paramMap.pipe(
    map(paramMap => paramMap.get('id')),
    distinctUntilChanged(),
    switchMap(id => this.socket.fromEvent<Message>(`chat-message-${id}`)),
    tap(message => this.chatFacade.addMessage(message)),
  );

  constructor(
    private chatFacade: ChatFacade,
    private route: ActivatedRoute,
    private authFacade: AuthFacade,
    private socket: Socket,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.chatFacade.getRoom(this.route.snapshot.params.id);
  }

  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }

  sendMessage(senderId: string, value: MessageFormValue) {
    const { message } = value;
    if (!message) {
      return;
    }
    const { id } = this.route.snapshot.params;
    this.chatFacade.createMessage(id, senderId, message);
    this.messageForm.reset();
  }
}
