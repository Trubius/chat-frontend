import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';
import { ChatRoutingModule } from './chat-routing.module';
import { ChatRoomListComponent } from './pages/chat-room-list/chat-room-list.component';
import { ChatRoomComponent } from './pages/chat-room/chat-room.component';
import { ChatStoreModule } from './store';

@NgModule({
  declarations: [ChatRoomListComponent, ChatRoomComponent],
  imports: [CommonModule, ChatRoutingModule, SharedModule, ReactiveFormsModule, ChatStoreModule],
})
export class ChatModule {}
