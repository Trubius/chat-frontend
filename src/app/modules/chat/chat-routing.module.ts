import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckUserGuard } from '@shared/guards/check-user.guard';
import { ChatRoomListComponent } from './pages/chat-room-list/chat-room-list.component';
import { ChatRoomComponent } from './pages/chat-room/chat-room.component';

const routes: Routes = [
  {
    path: '',
    component: ChatRoomListComponent,
    canActivate: [CheckUserGuard],
  },
  {
    path: ':id',
    component: ChatRoomComponent,
    canActivate: [CheckUserGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChatRoutingModule {}
