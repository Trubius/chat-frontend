import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthFacade } from '../../store/facade';

interface LoginFormValue {
  email: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  constructor(private authFacade: AuthFacade, private router: Router) {}

  login(value: LoginFormValue) {
    const { email, password } = value;
    this.authFacade.loginUser(email, password);
  }

  async navigateToSignUp() {
    return this.router.navigate(['signup']);
  }
}
