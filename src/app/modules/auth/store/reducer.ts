import { Action, createReducer, on } from '@ngrx/store';
import * as authActions from './actions';
import { AuthState, initialAuthState } from './state';

export const reducer = createReducer(
  initialAuthState,
  on(authActions.getLoggedUserSuccess, (state, { user }) => ({
    ...state,
    user,
  })),
  on(authActions.getLoggedUserFail, state => ({
    ...state,
    user: null,
  })),
  on(authActions.logoutUserSuccess, () => ({
    ...initialAuthState,
  })),
);

export function authReducer(state: AuthState, action: Action) {
  return reducer(state, action);
}
