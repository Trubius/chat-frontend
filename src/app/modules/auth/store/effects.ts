import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ChatApiService } from '@app/services/api/chat-api.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Socket } from 'ngx-socket-io';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import * as authActions from './actions';

@Injectable()
export class AuthEffects {
  loginUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.loginUser),
      switchMap(({ email, password }) => {
        return this.chatApiService.loginUser(email, password).pipe(
          map(() => authActions.getLoggedUser({ navigateToRooms: true })),
          catchError((error: HttpErrorResponse) => of(authActions.loginUserFail({ error: error.message }))),
        );
      }),
    ),
  );

  logoutUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.logoutUser),
      switchMap(() => {
        return this.chatApiService.logoutUser().pipe(
          map(({ status }) => authActions.logoutUserSuccess({ status })),
          catchError((error: HttpErrorResponse) => of(authActions.logoutUserFail({ error: error.message }))),
        );
      }),
    ),
  );

  logoutUserSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.logoutUserSuccess),
        tap(() => {
          this.socket.emit('logout');
          void this.router.navigate(['login']);
        }),
      ),
    { dispatch: false },
  );

  getLoggedUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.getLoggedUser),
      switchMap(({ navigateToRooms }) => {
        return this.chatApiService.getLoggedUser().pipe(
          map(user => authActions.getLoggedUserSuccess({ user, navigateToRooms })),
          catchError((error: HttpErrorResponse) => of(authActions.getLoggedUserFail({ error: error.message }))),
        );
      }),
    ),
  );

  getLoggedUserSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.getLoggedUserSuccess),
        tap(({ user, navigateToRooms }) => {
          this.socket.emit('login', { userId: user.id });
          if (navigateToRooms) {
            void this.router.navigate(['rooms']);
          }
        }),
      ),
    { dispatch: false },
  );

  constructor(
    private actions$: Actions,
    private chatApiService: ChatApiService,
    private router: Router,
    private socket: Socket,
  ) {}
}
