import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as authActions from './actions';
import * as authSelectors from './selectors';

@Injectable({ providedIn: 'root' })
export class AuthFacade {
  user$ = this.store.select(authSelectors.getUser);

  constructor(private store: Store) {}

  loginUser(email: string, password: string) {
    this.store.dispatch(authActions.loginUser({ email, password }));
  }

  logoutUser() {
    this.store.dispatch(authActions.logoutUser());
  }

  getLoggedUser(navigateToRooms = true) {
    this.store.dispatch(authActions.getLoggedUser({ navigateToRooms }));
  }
}
