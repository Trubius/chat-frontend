import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState } from './state';

export const AUTH_STORE_KEY = 'auth';

const featureSelector = createFeatureSelector<AuthState>(AUTH_STORE_KEY);

export const getUser = createSelector(featureSelector, state => state.user);
