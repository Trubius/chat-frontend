import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AuthEffects } from './effects';
import { authReducer } from './reducer';
import { AUTH_STORE_KEY } from './selectors';

@NgModule({
  imports: [CommonModule, StoreModule.forFeature(AUTH_STORE_KEY, authReducer), EffectsModule.forFeature([AuthEffects])],
})
export class AuthStoreModule {}
