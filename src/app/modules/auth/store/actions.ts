import { createAction, props } from '@ngrx/store';
import { User } from '@shared/interfaces/user';

export const loginUser = createAction('[Auth] Login User Fetch', props<{ email: string; password: string }>());
export const loginUserSuccess = createAction('[Auth] Login User Success', props<{ status: string }>());
export const loginUserFail = createAction('[Auth] Login User Fail', props<{ error: string }>());

export const logoutUser = createAction('[Auth] Logout User Fetch');
export const logoutUserSuccess = createAction('[Auth] Logout User Success', props<{ status: string }>());
export const logoutUserFail = createAction('[Auth] Logout User Fail', props<{ error: string }>());

export const getLoggedUser = createAction('[Auth] Get Logged User Fetch', props<{ navigateToRooms: boolean }>());
export const getLoggedUserSuccess = createAction(
  '[Auth] Get Logged User Success',
  props<{ user: User; navigateToRooms: boolean }>(),
);
export const getLoggedUserFail = createAction('[Auth] Get Logged User Fail', props<{ error: string }>());
