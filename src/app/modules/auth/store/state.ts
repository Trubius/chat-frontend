import { User } from '@shared/interfaces/user';

export const initialAuthState: AuthState = {
  user: undefined,
};

export interface AuthState {
  user?: User | null;
}
