import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';
import { SignupComponent } from './pages/signup/signup.component';
import { SignupRoutingModule } from './signup-routing.module';
import { SignUpStoreModule } from './store';

@NgModule({
  declarations: [SignupComponent],
  imports: [CommonModule, SignupRoutingModule, SharedModule, ReactiveFormsModule, SignUpStoreModule],
})
export class SignupModule {}
