import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ChatApiService } from '@app/services/api/chat-api.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as signUpActions from './actions';

@Injectable()
export class SignUpEffects {
  createUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(signUpActions.createUser),
      switchMap(({ name, email, password }) => {
        return this.chatApiService.createUser(name, email, password).pipe(
          map(({ status }) => signUpActions.createUserSuccess({ status })),
          catchError((error: HttpErrorResponse) => of(signUpActions.createUserFail({ error: error.message }))),
        );
      }),
    ),
  );

  createUserSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(signUpActions.createUserSuccess),
        map(async () => this.router.navigate(['login'])),
      ),
    { dispatch: false },
  );

  constructor(private actions$: Actions, private chatApiService: ChatApiService, private router: Router) {}
}
