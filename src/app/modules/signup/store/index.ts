import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { SignUpEffects } from './effects';
import { SignUpFacade } from './facade';

@NgModule({
  imports: [CommonModule, EffectsModule.forFeature([SignUpEffects])],
  providers: [SignUpFacade],
})
export class SignUpStoreModule {}
