import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as authActions from './actions';

@Injectable()
export class SignUpFacade {
  constructor(private store: Store) {}

  createUser(name: string, email: string, password: string) {
    this.store.dispatch(authActions.createUser({ name, email, password }));
  }
}
