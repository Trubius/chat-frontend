import { createAction, props } from '@ngrx/store';

export const createUser = createAction(
  '[SignUp] Create User Fetch',
  props<{ name: string; email: string; password: string }>(),
);
export const createUserSuccess = createAction('[SignUp] Create User Success', props<{ status: string }>());
export const createUserFail = createAction('[SignUp] Create User Fail', props<{ error: string }>());
