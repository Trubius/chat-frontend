import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { SignUpFacade } from '@modules/signup/store/facade';

interface SignUpFormValue {
  name: string;
  email: string;
  password: string;
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent {
  signUpForm = new FormGroup({
    name: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl(''),
  });

  constructor(private signUpFacade: SignUpFacade, private router: Router) {}

  signUp(value: SignUpFormValue) {
    const { name, email, password } = value;
    this.signUpFacade.createUser(name, email, password);
  }

  async navigateToLogin() {
    return this.router.navigate(['login']);
  }
}
